FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 80

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src
COPY [".\webapp\webapp.csproj", ""]
RUN dotnet restore "webapp.csproj"
COPY . .
WORKDIR "/src/"
RUN dotnet build "webapp.csproj" -c Release -o /app
