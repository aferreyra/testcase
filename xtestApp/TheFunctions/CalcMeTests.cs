using Moq;
using System;
using Xunit;
using webapp.TheFunctions;

namespace xtestApp.TheFunctions
{
    public class CalcMeTests : IDisposable
    {
        private MockRepository mockRepository;



        public CalcMeTests()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);


        }

        public void Dispose()
        {
            this.mockRepository.VerifyAll();
        }

        private CalcMe CreateCalcMe()
        {
            return new CalcMe();
        }

        [Fact]
        public void Add_StateUnderTest_ExpectedBehavior()
        {
            // Arrange
            var calcMe = this.CreateCalcMe();
            int i = 0;
            int x = 0;

            // Act
            var result = calcMe.Add(
                i,
                x);

            // Assert
            Assert.True(result == 0);
        }
    }
}
